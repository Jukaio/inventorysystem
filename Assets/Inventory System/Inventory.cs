using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Pest.UI
{
	using InventoryListener;

	/* TODO: Make base class that deals with UI interaction
	 * what would allow different user interface classes 
	 * using the same base. It would be rad. */
	/* TODO: Instead of inheritance use composition
	 * since composition with listeners should work even better
	 * than any form of inheritance in these regards */

	[DisallowMultipleComponent]
	[RequireComponent(typeof(RectTransform), typeof(GridLayoutGroup))]
	public abstract partial class Inventory : MonoBehaviour, IUserInterfaceElement
	{
		public enum DeviceMode
		{
			Pointer, // Navigate with mouse
			Cursor,  // Navigate with directions
		}

		[SerializeField] private Camera cam;
		[SerializeField] private GameObject slot;
		[SerializeField] private Vector2Int size;

		private DeviceMode mode = DeviceMode.Pointer;
		private GridLayoutGroup gridLayoutGroup;
		private RectTransform rectTransform;
		private Slot[,] slots = null;
		private Slot dragStartSlot = null;
		private Slot prevHoveredSlot = null;

		private Item heldItem = null;

		private Vector2 mousePosition = Vector2.zero;
		private Vector2Int cursorIndex = Vector2Int.zero;
		private bool isSplitting = false;
		private bool isSeparating = false;

		private HashSet<ILifetimeListener> lifetimeListeners = new HashSet<ILifetimeListener>();
		private HashSet<IInteractionListener> interactionListeners = new HashSet<IInteractionListener>();

		public delegate bool SlotActionRefIndex(ref Slot slot, Vector2Int index);
		public delegate bool SlotActionRef(ref Slot slot);

		// Public Interface

		public void Register(ILifetimeListener listener)
		{
			if (lifetimeListeners.Contains(listener)) {
				return;
			}

			lifetimeListeners.Add(listener);
		}
		public void Register(IInteractionListener listener)
		{
			if (interactionListeners.Contains(listener)) {
				return;
			}

			interactionListeners.Add(listener);
		}
		public void Unregister(ILifetimeListener listener)
		{
			if (!lifetimeListeners.Contains(listener)) {
				return;
			}

			lifetimeListeners.Remove(listener);
		}
		public void Unregister(IInteractionListener listener)
		{
			if (!interactionListeners.Contains(listener)) {
				return;
			}

			interactionListeners.Remove(listener);
		}

		public void Construct()
		{
			rectTransform = GetComponent<RectTransform>();
			gridLayoutGroup = GetComponent<GridLayoutGroup>();

			if (slots == null) {
				Initalise();
				Resize();

				foreach (var item in lifetimeListeners) {
					item.OnUserCreate();
				}
			}
		}

		/* TOODO: More functionality and overloads 
		 * for the user who inherits this class */
		public bool Store(Item that)
		{
			if (that is StackableItem) {
				if (Has(that, out var at)) {
					var otherItem = GetSlot(at).GetItem() as StackableItem;
					otherItem.Merge(that as StackableItem);
					return true;
				}
			}

			bool stored = false;
			ForEach((ref Slot slot) =>
			{
				if (slot.IsEmpty()) {
					slot.Replace(that);
					stored = true;
					return false;
				}
				return true;
			});
			return stored;
		}
		public bool Has(Item that, out Vector2Int at)
		{
			for (int y = 0; y < size.y; y++) {
				for (int x = 0; x < size.x; x++) {
					var index = new Vector2Int(x, y);
					var otherItem = GetSlot(index)?.GetItem();
					if (otherItem != null ? IsSameType(that, otherItem) : false) {
						at = index;
						return true;
					}
				}
			}
			at = new Vector2Int(-1, -1);
			return false;
		}
		public bool IsHoldingItem()
		{
			return heldItem != null;
		}

		public Vector2Int ClampToInventory(Vector2Int index)
		{
			index.x = Mathf.Clamp(index.x, 0, slots.GetLength(0) - 1);
			index.y = Mathf.Clamp(index.y, 0, slots.GetLength(1) - 1);
			return index;
		}
		public Vector2Int ScreenToInventory(Vector2 coordinate)
		{
			if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, coordinate, cam, out Vector2 point)) {
				point.y = -point.y; // invert y-coordinate cause, idk... Unity
				var cellSize = gridLayoutGroup.cellSize;
				var index = new Vector2Int((int)(point.x / cellSize.x), (int)(point.y / cellSize.y));
				return index;
			}
			return new Vector2Int(-1, -1);
		}
		public Vector2 InventoryToScreen(Vector2Int index)
		{
			var cellSize = gridLayoutGroup.cellSize;
			var point = new Vector2(index.x * cellSize.x, index.y * cellSize.y);
			var world = rectTransform.TransformPoint(point);
			return cam.WorldToScreenPoint(new Vector3(world.x, -world.y, 0.0f));
		}
		public Vector2Int ScreenToClampedInventoryPosition(Vector2 position)
		{
			var pos = ScreenToInventory(position);
			return ClampToInventory(pos);
		}
		public Slot GetSlot(Vector2Int index)
		{
			if (IsIndexInBounds(index)) {
				return slots[index.x, index.y];
			}
			return null;
		}
		public Slot GetClampedSlot(Vector2Int index)
		{
			index = ClampToInventory(index);
			return GetSlot(index);
		}

		public void ForEach(SlotActionRefIndex breakAction)
		{
			for (int y = 0; y < size.y; y++) {
				for (int x = 0; x < size.x; x++) {
					var index = new Vector2Int(x, y);
					var slot = GetSlot(index);
					if (!breakAction(ref slot, index)) {
						return;
					}
				}
			}
		}
		public void ForEach(SlotActionRef breakAction)
		{
			for (int y = 0; y < size.y; y++) {
				for (int x = 0; x < size.x; x++) {
					var index = new Vector2Int(x, y);
					var slot = GetSlot(index);
					if (!breakAction(ref slot)) {
						return;
					}
				}
			}
		}
		// !Public Interface


		// Private Interface

		private void Initalise()
		{
			slots = new Slot[size.x, size.y];
			for (int y = 0; y < size.y; y++) {
				for (int x = 0; x < size.x; x++) {
					var child = Instantiate(slot, transform);
					slots[x, y] = child.GetComponent<Slot>();
				}
			}
		}
		private void Resize()
		{
			var slot_size = slot.GetComponent<RectTransform>().rect;
			var inventoryDimensions = new Vector2(slot_size.width * size.x, slot_size.height * size.y);
			rectTransform.sizeDelta = inventoryDimensions;
		}
		private void Hover(Vector2Int index)
		{
			if (!IsIndexInBounds(index)) {
				prevHoveredSlot?.Unhover();
				prevHoveredSlot = null;
				return;
			}

			Slot currentSlot = GetSlot(index);
			if (currentSlot != prevHoveredSlot) {
				prevHoveredSlot?.Unhover();
				currentSlot.Hover();
				prevHoveredSlot = currentSlot;
			}
		}
		private void DragStart()
		{
			if (!IsIndexInBounds(cursorIndex)) {
				return;
			}

			dragStartSlot = GetSlot(cursorIndex);
			if (dragStartSlot == null) {
				return;
			}

			if (dragStartSlot.ReceiveItem(out var item)) {
				if (item is StackableItem) {
					GrabStackableItem(item as StackableItem);
				}
				else {
					GrabItem(item);
				}

				DispatchDragStart(heldItem);
			}
		}
		private void GrabStackableItem(StackableItem item)
		{
			if (item.GetSize() > 1) {
				if (isSplitting == true) {
					heldItem = item.Split();
					return;
				}
				else if (isSeparating == true) {
					heldItem = item.Separate();
					return;
				}
			}
			GrabItem(item);
		}
		private void GrabItem(Item item)
		{
			heldItem = item;
			dragStartSlot.Replace(null);
		}
		private void DragEnd()
		{
			if (dragStartSlot == null) {
				return;
			}

			var to = GetSlot(cursorIndex);


			if (to != null) {
				Swap(to);
			}
			else {
				if (heldItem is StackableItem) {
					ResetStackableItem();
				}
				else {
					ResetItem();
				}
			}

			DispatchDragEnd(to);

			dragStartSlot = null;
			heldItem = null;
		}

		private void Swap(Slot to)
		{
			if (heldItem == null || dragStartSlot == null || to == null) {
				return;
			}

			if (to.ReceiveItem(out var other)) {
				if (heldItem is StackableItem) {
					if (IsSameType(heldItem, other)) {
						var stackableItem = heldItem as StackableItem;
						var otherStackableItem = other as StackableItem;
						otherStackableItem.Merge(stackableItem);
						heldItem = otherStackableItem;
						return;
					}
				}
				dragStartSlot.Replace(other);
			}
			to.Replace(heldItem);
		}
		private void ResetStackableItem()
		{
			if (dragStartSlot.ReceiveItem(out var item)) {
				var stackableItem = item as StackableItem;
				stackableItem.Merge(heldItem.OnOutOfWindow() as StackableItem);
				return;
			}
			ResetItem();
		}
		private void ResetItem()
		{
			dragStartSlot.Replace(heldItem.OnOutOfWindow());
		}

		private bool IsSameType(Object lhs, Object rhs)
		{
			return lhs.GetType() == rhs.GetType();
		}
		private bool IsIndexInBounds(Vector2Int index)
		{
			return index.x >= 0 && index.x < size.x &&
				   index.y >= 0 && index.y < size.y;
		}

		private void SwitchControlScheme(DeviceMode to)
		{
			if (mode == to) {
				return;
			}
			mode = to;
		}

		private void OnApplicationQuit()
		{
			foreach (var item in lifetimeListeners) {
				item.OnUserDestroy();
			}
		}
		// !Private Interface

		// The following methods can get replaced with sensible struct constructors instead
		// It would also remove public setters : ) 
		private void DispatchSelection(Slot select)
		{
			IInteractionListener.SelectionContext selContext = new IInteractionListener.SelectionContext();
			selContext.deviceMode = mode;
			selContext.slot = select;
			selContext.mousePosition = mousePosition;
			selContext.hoveredPosition = cursorIndex;
			foreach (var item in interactionListeners) {
				item.OnUserSelect(selContext);
			}
		}
		private void DispatchNavigation()
		{
			IInteractionListener.NavigationContext navContext = new IInteractionListener.NavigationContext();
			navContext.deviceMode = mode;
			navContext.slot = GetSlot(cursorIndex);
			navContext.mousePosition = mousePosition;
			navContext.hoveredPosition = cursorIndex;
			foreach (var item in interactionListeners) {
				item.OnUserNavigate(navContext);
			}
		}
		private void DispatchDragStart(Item item)
		{
			IInteractionListener.DragStartContext dragStartContext = new IInteractionListener.DragStartContext();
			dragStartContext.deviceMode = mode;
			dragStartContext.startSlot = dragStartSlot;
			dragStartContext.item = item;
			dragStartContext.mousePosition = mousePosition;
			dragStartContext.hoveredPosition = cursorIndex;
			foreach (var element in interactionListeners) {
				element.OnUserDragStart(dragStartContext);
			}
		}
		private void DispatchDragOnGoing()
		{
			IInteractionListener.DragtOnGoingContext dragContext = new IInteractionListener.DragtOnGoingContext();
			dragContext.deviceMode = mode;
			dragContext.item = heldItem;
			dragContext.mousePosition = mousePosition;
			dragContext.hoveredPosition = cursorIndex;
			foreach (var item in interactionListeners) {
				item.OnUserDragOnGoing(dragContext);
			}
		}
		private void DispatchDragEnd(Slot to)
		{
			IInteractionListener.DragEndContext dragEndContext = new IInteractionListener.DragEndContext();
			dragEndContext.deviceMode = mode;
			dragEndContext.startSlot = dragStartSlot;
			dragEndContext.item = heldItem;
			dragEndContext.endSlot = to;
			dragEndContext.mousePosition = mousePosition;
			dragEndContext.hoveredPosition = cursorIndex;
			foreach (var element in interactionListeners) {
				element.OnUserDragEnd(dragEndContext);
			}
		}
	}
}