using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;

namespace Pest.UI
{
	public abstract partial class Inventory 
	{
		[DisallowMultipleComponent]
		public abstract class Item : MonoBehaviour
		{
			protected RectTransform rectTransform;

			/// <summary>
			/// Gets called when item gets dragged out of the window.
			/// </summary>
			/// <returns> A reference to the next item supposed to get stored in the inventory. 
			/// Return null to remove item from inventory </returns>
			public abstract Item OnOutOfWindow();

			/// <summary>
			/// Gets called when item is getting selected
			/// </summary>
			/// <returns> A reference to the item supposed toget stored in the inventory.
			/// Return null to remove item from inventory </returns>
			public abstract Item OnSelect();

			private void Awake()
			{
				rectTransform = transform as RectTransform;
			}

			public void SetParent(RectTransform parent)
			{
				rectTransform.SetParent(parent);
			}

			public void SetPosition(Vector2 position)
			{
				rectTransform.anchoredPosition = position;
			}

			public void SetScale(Vector2 scale)
			{
				rectTransform.localScale = scale;
			}

			public void SetPivot(Vector2 pivot)
			{
				rectTransform.pivot = pivot;
			}
		}
	}
}