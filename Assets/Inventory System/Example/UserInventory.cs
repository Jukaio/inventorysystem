﻿using Pest.UI;
using UnityEngine;
using Pest.UI.InventoryListener;

public class UserInventory : Inventory, ILifetimeListener, IInteractionListener
{
	[SerializeField] private GameObject defaultItem;
	[SerializeField] private GameObject defaultStackableItem;
	private RectTransform parentTransform;

	public void OnUserCreate()
	{
		Debug.Log("Creating");

		Store(Instantiate(defaultItem, transform).GetComponent<Item>());
		Store(Instantiate(defaultItem, transform).GetComponent<Item>());
		Store(Instantiate(defaultStackableItem, transform).GetComponent<Item>());
		Store(Instantiate(defaultStackableItem, transform).GetComponent<Item>());
		Store(Instantiate(defaultStackableItem, transform).GetComponent<Item>());
		Store(Instantiate(defaultStackableItem, transform).GetComponent<Item>());
		Store(Instantiate(defaultStackableItem, transform).GetComponent<Item>());
	}

	public void OnUserDestroy()
	{
		// meh.
	}

	public void OnUserDragStart(IInteractionListener.DragStartContext context)
	{
		var heldItem = context.item;

		heldItem.SetParent(parentTransform);
		
		switch (context.deviceMode) {
			case DeviceMode.Pointer:
				heldItem.SetPosition(context.mousePosition);
				break;
			case DeviceMode.Cursor:
				heldItem.SetPosition(InventoryToScreen(context.hoveredPosition));
				break;
		}

		heldItem.SetPivot(new Vector2(0.5f, 0.5f));
	}

	public void OnUserDragEnd(IInteractionListener.DragEndContext context)
	{
		
	}

	public void OnUserDragOnGoing(IInteractionListener.DragtOnGoingContext context)
	{
		var heldItem = context.item;
		switch (context.deviceMode) {
			case DeviceMode.Pointer:
				heldItem.SetPosition(context.mousePosition);
				break;
			case DeviceMode.Cursor:
				heldItem.SetPosition(InventoryToScreen(context.hoveredPosition));
				break;
		}

	}

	public void OnUserNavigate(IInteractionListener.NavigationContext context)
	{
		// Sound, particles and such stuff
	}

	public void OnUserPoint(IInteractionListener.NavigationContext context)
	{
		// Particles and such stuff
	}

	public void OnUserSelect(IInteractionListener.SelectionContext context)
	{
		var slot = context.slot;
		slot.Replace(slot.GetItem()?.OnSelect());
	}

	private void Awake()
	{
		parentTransform = transform.parent as RectTransform;
		
		Register(this as ILifetimeListener);
		Register(this as IInteractionListener);

		Construct();
	}
}

