﻿using Pest.UI;
using UnityEngine;
using UnityEngine.UI;

public class UserSlot : Inventory.Slot
{

	public override void OnHover(Image image, RectTransform transform)
	{
		image.color = Color.red;
	}

	public override void OnUnhover(Image image, RectTransform transform)
	{
		image.color = Color.white;
	}
}

