using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pest.UI;

public class SkullsItem : StackableItem
{
	[SerializeField] private Text text;

	public override Inventory.Item OnOutOfWindow()
	{
		return this;
	}

	public override StackableItem OnUse(StackableItem item)
	{
		Debug.Log("Do skelly stuff!");
		return item;
	}

	public override void OnCount(int count)
	{
		RefreshText(count);
	}

	private void RefreshText(int count)
	{
		text.text = "x" + count.ToString();
	}
}
