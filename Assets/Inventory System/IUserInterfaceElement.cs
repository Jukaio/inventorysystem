﻿using UnityEngine.InputSystem;

namespace Pest.UI
{
	public interface IUserInterfaceElement
	{
		void Construct();
		void OnClick(InputAction.CallbackContext context);
		void OnDrag(InputAction.CallbackContext context);
		void OnNavigate(InputAction.CallbackContext context);
		void OnPoint(InputAction.CallbackContext context);
		void OnSeparate(InputAction.CallbackContext context);
		void OnSplit(InputAction.CallbackContext context);
	}
}