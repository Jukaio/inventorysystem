﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Pest.UI
{
	public abstract partial class Inventory 
	{
		public void OnPoint(InputAction.CallbackContext context)
		{
			if (context.phase != InputActionPhase.Performed) {
				return;
			}

			SwitchControlScheme(DeviceMode.Pointer);

			mousePosition = context.ReadValue<Vector2>();
			cursorIndex = ScreenToInventory(mousePosition);

			DispatchNavigation();

			if (IsHoldingItem()) {
				DispatchDragOnGoing();
			}

			Hover(ScreenToInventory(mousePosition));
		}
		public void OnNavigate(InputAction.CallbackContext context)
		{
			if (context.phase == InputActionPhase.Performed) {

				SwitchControlScheme(DeviceMode.Cursor);

				var input = AxisToGridDirection(context.ReadValue<Vector2>());
				cursorIndex = ClampToInventory(cursorIndex + input);

				DispatchNavigation();

				if (IsHoldingItem()) {
					DispatchDragOnGoing();
				}

				Hover(cursorIndex);
			}
		}
		public void OnClick(InputAction.CallbackContext context)
		{
			if (context.phase == InputActionPhase.Performed) {
				if (!IsHoldingItem()) {
					if (IsIndexInBounds(cursorIndex)) {
						var select = GetSlot(cursorIndex);
						if (select == null) {
							return;
						}

						DispatchSelection(select);
					}
				}
			}
		}
		public void OnSplit(InputAction.CallbackContext context)
		{
			isSplitting = CheckState(context.phase);
		}
		public void OnSeparate(InputAction.CallbackContext context)
		{
			isSeparating = CheckState(context.phase);
		}
		public void OnDrag(InputAction.CallbackContext context)
		{
			switch (context.phase) {
				case InputActionPhase.Performed:
					DragStart();
					break;
				case InputActionPhase.Canceled:
					DragEnd();
					break;
			}
		}

		private bool CheckState(InputActionPhase phase)
		{
			switch (phase) {
				case InputActionPhase.Performed:
					return true;
				case InputActionPhase.Canceled:
					return false;
			}
			return false;
		}
		private Vector2Int AxisToGridDirection(Vector2 direction)
		{
			Vector2Int toReturn = Vector2Int.zero;
			toReturn.x = Mathf.RoundToInt(direction.x);
			toReturn.y = Mathf.RoundToInt(-direction.y);
			return toReturn;
		}
	}
}
