using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pest.UI
{
	public abstract class StackableItem : Inventory.Item
	{
		private List<StackableItem> items = new List<StackableItem>();
		public int Count => items.Count;

		/// <summary>
		/// Gets called when stack amount changes
		/// Used for UI mostly
		/// </summary>
		public abstract void OnCount(int count);

		/// <summary>
		/// Gets called when item is getting used. Usage is on selection.
		/// StackableItem takes care of removing it from the stack internally
		/// </summary>
		/// <returns> A reference to the item supposed toget stored in the stack.
		/// Return null to remove item from stack
		/// If the stack is zero, it gets removed from the inventory </returns>
		public abstract StackableItem OnUse(StackableItem item);

		public int GetSize()
		{
			return items.Count + 1;
		}

		internal void Merge(StackableItem that)
		{
			items.Add(that);
			items.AddRange(that.items);
			that.items.Clear();

			foreach (var i in items) {
				i.gameObject.SetActive(false);
				i.SetParent(rectTransform);
			}
			OnCount(items.Count + 1);
		}

		public override Inventory.Item OnSelect()
		{
			if (items.Count > 0) {
				var item = items[0];
				items.RemoveAt(0);
				item = OnUse(item);
				items.Add(item);
				return this;
			}
			return OnUse(this);
		}

		internal StackableItem Split()
		{
			var other = Separate();

			// Split children
			var count = items.Count / 2;
			other.items.AddRange(items.GetRange(0, count));
			items.RemoveRange(0, count);

			OnCount(items.Count + 1);
			other.OnCount(other.items.Count + 1);
			return other;
		}

		internal StackableItem Separate()
		{
			if (!(items.Count > 0)) {
				return null;
			}

			// Get itself (Next parent)
			var other = items[0];
			items.RemoveAt(0); // Remove the next parent

			other.gameObject.SetActive(true);
			OnCount(items.Count + 1);
			other.OnCount(other.items.Count + 1);
			return other;
		}
	}
}