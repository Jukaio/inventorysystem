﻿using UnityEngine;

namespace Pest.UI
{
	namespace InventoryListener
	{
		public enum DragState
		{
			Start,
			Ongoing,
			End
		}

		public interface ILifetimeListener
		{
			void OnUserCreate();
			void OnUserDestroy();
		}

		public interface IInteractionListener
		{
			public struct NavigationContext
			{
				public Inventory.DeviceMode deviceMode;
				public Inventory.Slot slot;
				public Vector2 mousePosition;
				public Vector2Int hoveredPosition;
			}
			public struct SelectionContext
			{
				public Inventory.DeviceMode deviceMode;
				public Inventory.Slot slot;
				public Vector2 mousePosition;
				public Vector2Int hoveredPosition;
			}
			public struct DragStartContext
			{
				public Inventory.DeviceMode deviceMode;
				public Inventory.Slot startSlot;
				public Inventory.Item item;
				public Vector2 mousePosition;
				public Vector2Int hoveredPosition;
			}
			public struct DragtOnGoingContext
			{
				public Inventory.DeviceMode deviceMode;
				public Inventory.Item item;
				public Vector2 mousePosition;
				public Vector2Int hoveredPosition;
			}
			public struct DragEndContext
			{
				public Inventory.DeviceMode deviceMode;
				public Inventory.Slot startSlot;
				public Inventory.Slot endSlot;
				public Inventory.Item item;
				public Vector2 mousePosition;
				public Vector2Int hoveredPosition;
			}

			void OnUserPoint(NavigationContext context);
			void OnUserNavigate(NavigationContext context);	
			void OnUserSelect(SelectionContext context);
			void OnUserDragStart(DragStartContext context);
			void OnUserDragOnGoing(DragtOnGoingContext context);
			void OnUserDragEnd(DragEndContext context);
		}

	}
}
