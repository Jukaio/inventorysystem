using Pest;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pest.UI
{
	public abstract partial class Inventory
	{
		public abstract class Slot : MonoBehaviour
		{
			private Item item = null;
			private RectTransform rectTransform;
			private Image image;

			public abstract void OnHover(Image image, RectTransform transform);
			public abstract void OnUnhover(Image image, RectTransform transform);

			private void Awake()
			{
				image = GetComponent(typeof(Image)) as Image;
				rectTransform = transform as RectTransform;
			}

			public bool IsEmpty()
			{
				return item == null;
			}

			public bool ReceiveItem(out Item item)
			{
				item = this.item;
				return item != null;
			}
			public Item GetItem()
			{
				return item;
			}

			public bool Swap(Slot with)
			{
				if (item == null) {
					return false;
				}
				var temp = with.Replace(item);
				Replace(temp);
				return true;
			}

			public bool Stack(Slot with)
			{
				if (item == null) {
					return false;
				}

				if(item is StackableItem && with.item is StackableItem) {
					var stackableItem = item as StackableItem;
					var otherStackableItem = with.item as StackableItem;
					otherStackableItem.Merge(stackableItem);
					return true;
				}

				return false;
			}

			public void Hover()
			{
				OnHover(image, rectTransform);
			}

			public void Unhover()
			{
				OnUnhover(image, rectTransform);
			}

			// Returns old item held
			public Item Replace(Item item)
			{
				Item temp = this.item;
				this.item = item;
				if (this.item != null) {
					this.item.SetParent(rectTransform);
					this.item.SetPosition(Vector3.zero);
					this.item.SetPivot(Vector2.zero);
				}
				return temp;
			}
		}
	}
}
